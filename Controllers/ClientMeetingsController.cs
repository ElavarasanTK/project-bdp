﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BDPlanning.Models;

namespace BDPlanning.Controllers
{
    public class ClientMeetingsController : Controller
    {
        private readonly BDPlanningContext _context;

        public ClientMeetingsController(BDPlanningContext context)
        {
            _context = context;
        }

        // GET: ClientMeetings
        public async Task<IActionResult> Index()
        {
            var bDPlanningContext = _context.ClientMeeting.Where(c=>c.DeleteFlag==false)
                .Include(c => c.Client).Include(c => c.MeetingType).Include(c => c.User);
            return View(await bDPlanningContext.ToListAsync());
        }

        // GET: ClientMeetings/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var clientMeeting = await _context.ClientMeeting
                .Include(c => c.Client)
                .Include(c => c.MeetingType)
                .Include(c => c.User)
                .SingleOrDefaultAsync(m => m.MeetingId == id);
            if (clientMeeting == null)
            {
                return NotFound();
            }

            return View(clientMeeting);
        }

        // GET: ClientMeetings/Create
        public IActionResult Create()
        {
            if (TempData["userEmail"] != null)
            {
                TempData.Keep("userEmail");
                TempData.Keep("userName");

                LoadDropDown();

                return View();
            }
            else
            {
                return RedirectToAction("Login", "Users");
            }
            
        }

        // POST: ClientMeetings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("MeetingId,ClientId,Location,MeetingPoc,MeetingPocEmail,MeetingPocNumber,MeetingPocDesig,UserId,MeetingTypeId,MeetingMom,HasRemainderFlag,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn,MeetingDate,MeetingName,RemainderOn")] ClientMeeting clientMeeting)
        {
            if (ModelState.IsValid)
            {
                clientMeeting.CreatedBy = TempData["userEmail"].ToString();
                clientMeeting.CreatedOn = DateTime.Now;
                clientMeeting.DeleteFlag = false;

                _context.Add(clientMeeting);
                var id = await _context.SaveChangesAsync();

                //Save remainder date
                //ClientMeetingRemainder objMeetingRemainder = new ClientMeetingRemainder();
                //objMeetingRemainder.ClientMeetingId = id;
                //objMeetingRemainder.ClientMeetingRemainderOn = clientMeeting.RemainderOn;
                //objMeetingRemainder.CreatedOn = DateTime.Now;
                //objMeetingRemainder.CreatedBy = TempData["userEmail"].ToString();
                //objMeetingRemainder.UpdatedOn = DateTime.Now;
                //objMeetingRemainder.UpdatedBy = TempData["userEmail"].ToString();
                //_context.Add(objMeetingRemainder);
                //await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            LoadDropDown();
            return View(clientMeeting);
        }




        // GET: ClientMeetings/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (TempData["userEmail"] != null)
            {
                TempData.Keep("userEmail");
                TempData.Keep("userName");

                if (id == null)
                {
                    return NotFound();
                }

                var clientMeeting = await _context.ClientMeeting.SingleOrDefaultAsync(m => m.MeetingId == id);
                if (clientMeeting == null)
                {
                    return NotFound();
                }
                LoadDropDown();
                //ViewBag.ClientId = new SelectList(ViewBag.Client, "ClientId", "ClientName", clientMeeting.ClientId);
                //ViewBag.MeetingTypeId = new SelectList(ViewBag.MeetingType, "MeetingTypeId", "MeetingTypeName", clientMeeting.MeetingTypeId);
                //ViewBag.UserId = new SelectList(ViewBag.User, "UserId", "UserName", clientMeeting.UserId);
                //ViewData["ClientId"] = new SelectList(_context.Client, "ClientId", "ClientName", clientMeeting.ClientId);
                //ViewData["MeetingTypeId"] = new SelectList(_context.MeetingType, "MeetingTypeId", "MeetingTypeId", clientMeeting.MeetingTypeId);
                //ViewData["UserId"] = new SelectList(_context.Users, "UserId", "CreatedBy", clientMeeting.UserId);
                return View(clientMeeting);
            }
            else
            {
                return RedirectToAction("Login", "Users");
            }
            
        }

        // POST: ClientMeetings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("MeetingId,ClientId,Location,MeetingPoc,MeetingPocEmail,MeetingPocNumber,MeetingPocDesig,UserId,MeetingTypeId,MeetingMom,UpdatedBy,UpdatedOn,MeetingDate,MeetingName,CreatedOn,CreatedBy")] ClientMeeting clientMeeting)
        {
            if (id != clientMeeting.MeetingId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    
                    clientMeeting.UpdatedBy = TempData["userEmail"].ToString();
                    clientMeeting.UpdatedOn = DateTime.Now;
                    _context.Update(clientMeeting);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ClientMeetingExists(clientMeeting.MeetingId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            LoadDropDown();
            return View(clientMeeting);
        }

        // GET: ClientMeetings/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (TempData["userEmail"] != null)
            {
                TempData.Keep("userEmail");
                TempData.Keep("userName");
                if (id == null)
                {
                    return NotFound();
                }

                var clientMeeting = await _context.ClientMeeting
                          .SingleOrDefaultAsync(m => m.MeetingId == id);
                if (clientMeeting == null)
                {
                    return NotFound();
                }
                clientMeeting.DeleteFlag = true;
                clientMeeting.UpdatedBy = TempData["userEmail"].ToString();
                clientMeeting.UpdatedOn = DateTime.Now;
                _context.Update(clientMeeting);
                _context.SaveChanges();
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction("Login", "Users");
            }
        }

        

        private bool ClientMeetingExists(int id)
        {
            return _context.ClientMeeting.Any(e => e.MeetingId == id);
        }


        //Load drop downs
        public void LoadDropDown()
        {
            //load client drop down client, meeting type and users
            List<Client> lstClient = new List<Client>();
            List<MeetingType> lstMeeting = new List<MeetingType>();
            List<Users> lstUser = new List<Users>();

            var objClient = _context.Client
                            .Where(r => r.ClientActiveFlag == true)
                            .Select(r => new { r.ClientId, r.ClientName }).ToList();

            var objMeeting = _context.MeetingType
                            .Select(r => new { r.MeetingTypeId, r.MeetingTypeName }).ToList();

            
            var objUser = _context.Users
                          .Where(r => r.UserActiveFlag == true)
                          .Select(r => new { r.UserId, r.UserName }).ToList();

            if (objClient.Count > 0)
            {

                Client obClient = new Client();
                obClient.ClientId = 0;
                obClient.ClientName = "--Select Client--";
                lstClient.Add(obClient);

                foreach (var r in objClient)
                {
                    Client tmpClient = new Client();
                    tmpClient.ClientId = r.ClientId;
                    tmpClient.ClientName = r.ClientName;
                    lstClient.Add(tmpClient);
                }
            }
            else
            {
                Client obClient = new Client();
                obClient.ClientId = 0;
                obClient.ClientName = "--No clients to display--";
                lstClient.Add(obClient);
            }

            if (objMeeting.Count > 0)
            {
                MeetingType obMeeting = new MeetingType();
                obMeeting.MeetingTypeId = 0;
                obMeeting.MeetingTypeName = "--Select Meeting Type--";
                lstMeeting.Add(obMeeting);

                foreach (var r in objMeeting)
                {
                    MeetingType tmpMeeting = new MeetingType();
                    tmpMeeting.MeetingTypeId = r.MeetingTypeId;
                    tmpMeeting.MeetingTypeName = r.MeetingTypeName;
                    lstMeeting.Add(tmpMeeting);
                }
            }
            else
            {
                MeetingType obMeeting = new MeetingType();
                obMeeting.MeetingTypeId = 0;
                obMeeting.MeetingTypeName = "--No meeting type to display--";
                lstMeeting.Add(obMeeting);
            }

            if (objUser.Count > 0)
            {
                Users obUser = new Users();
                obUser.UserId = 0;
                obUser.UserName = "--Select User--";
                lstUser.Add(obUser);

                foreach (var r in objUser)
                {
                    Users tmpUser = new Users();
                    tmpUser.UserId = r.UserId;
                    tmpUser.UserName = r.UserName;
                    lstUser.Add(tmpUser);
                }
            }
            else
            {
                Users obUser = new Users();
                obUser.UserId = 0;
                obUser.UserName = "--No User to display--";
                lstUser.Add(obUser);
            }

            ViewBag.Client = lstClient;
            ViewBag.MeetingType = lstMeeting;
            ViewBag.User = lstUser;

           
        }

    }
}
