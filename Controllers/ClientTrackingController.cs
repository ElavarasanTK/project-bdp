﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BDPlanning.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq.Dynamic.Core;


namespace BDPlanning.Controllers
{
    public class ClientTrackingController : Controller
    {
        private readonly BDPlanningContext _context;

        public ClientTrackingController(BDPlanningContext context)
        {
            _context = context;
        }

       

        // GET: ClientTracking
        public IActionResult CreateClientTracking()
        {
            if(TempData["userEmail"]!=null)
            {
                TempData.Keep("userEmail");
                TempData.Keep("userName");

                LoadDropDown(); 

                return View();
            }
            else
            {
                return RedirectToAction("Login", "Users");
            }
        }

        [HttpPost]
        public IActionResult CreateClientTracking([Bind("ClientId,TrackingStateId,TrackingStatusId,Comment,AssignedTo,RemainderOn,CallDate")] ClientTrackingInfo clientTrack)
        {
            if (TempData["userEmail"] != null) 
            {
                clientTrack.CreatedBy = TempData["userEmail"].ToString();
                clientTrack.CreatedOn = DateTime.Now;
              
                _context.Add(clientTrack);
                var id = _context.SaveChanges();

                //Create remainder for client tracking
                ClientTrackingRemainder objClientTrackRemainder = new ClientTrackingRemainder();
                objClientTrackRemainder.ClientTrackingId = id;
                objClientTrackRemainder.ClientTrackingRemainderOn = clientTrack.RemainderOn;
                objClientTrackRemainder.CreatedBy = TempData["userEmail"].ToString();
                objClientTrackRemainder.CreatedOn = DateTime.Now;
                objClientTrackRemainder.UpdatedBy = TempData["userEmail"].ToString();
                objClientTrackRemainder.UpdatedOn = DateTime.Now;

               
                _context.Add(objClientTrackRemainder);
                _context.SaveChanges();

                LoadDropDown();

               return View("CreateClientTracking");


            }

            else
            {
                return RedirectToAction("Login", "Users");
            }
        }

        //Load drop downs
        public void LoadDropDown ()
        {
            //load client drop down, state, status and users
            List<Client> lstClient = new List<Client>();
            List<ClientTrackingStatus> lstStatus = new List<ClientTrackingStatus>();
            List<ClientTrackingState> lstState = new List<ClientTrackingState>();
            List<Users> lstUser = new List<Users>();

            Client obClient = new Client();
            obClient.ClientId = 0;
            obClient.ClientName = "--Select Client--";
            lstClient.Add(obClient);

            ClientTrackingStatus obStatus = new ClientTrackingStatus();
            obStatus.ClientTrackingStatusId = 0;
            obStatus.ClientTrackingStatusName = "--Select Tracking Status--";
            lstStatus.Add(obStatus);

            ClientTrackingState obState = new ClientTrackingState();
            obState.ClientTrackingStateId = 0;
            obState.ClientTrackingStateName = "--Select Traking State--";
            lstState.Add(obState);

            Users obUser = new Users();
            obUser.UserId = 0;
            obUser.UserName = "--Select User--";
            lstUser.Add(obUser);

            var objClient = _context.Client
                            .Where(r => r.ClientActiveFlag == true)
                            .Select(r => new { r.ClientId, r.ClientName }).ToList();

            var objStatus = _context.ClientTrackingStatus
                            .Select(r => new { r.ClientTrackingStatusId, r.ClientTrackingStatusName }).ToList();

            var objState = _context.ClientTrackingState
                           .Select(r => new { r.ClientTrackingStateId, r.ClientTrackingStateName }).ToList();

            var objUser = _context.Users
                          .Where(r => r.UserActiveFlag == true)
                          .Select(r => new { r.UserId, r.UserName }).ToList();

           
            foreach(var r in objClient)
            {
                Client tmpClient = new Client();
                tmpClient.ClientId = r.ClientId;
                tmpClient.ClientName = r.ClientName;
                lstClient.Add(tmpClient);
            }
            foreach (var r in objState)

            {
                ClientTrackingState tmpState = new ClientTrackingState();
                tmpState.ClientTrackingStateId = r.ClientTrackingStateId;
                tmpState.ClientTrackingStateName = r.ClientTrackingStateName;
                lstState.Add(tmpState);
            }
            foreach (var r in objStatus)
            {
                ClientTrackingStatus tmpStatus = new ClientTrackingStatus();
                tmpStatus.ClientTrackingStatusId = r.ClientTrackingStatusId;
                tmpStatus.ClientTrackingStatusName = r.ClientTrackingStatusName;
                lstStatus.Add(tmpStatus);
            }
            foreach (var r in objUser)
            {
                Users tmpUser = new Users();
                tmpUser.UserId = r.UserId;
                tmpUser.UserName = r.UserName;
                lstUser.Add(tmpUser);
            }

            ViewBag.lstClient = lstClient;
            ViewBag.lstStatus = lstStatus;
            ViewBag.lstState = lstState;
            ViewBag.lstUsers = lstUser;
        }
        //bind with client tracking table
        //ajax call for loading client grid 
        //bind view with client tracking table
        //bind view with client tracking table

        [HttpPost]
        public IActionResult LoadClientGrid(int? clientId)
        {   

            var draw = HttpContext.Request.Form["draw"].FirstOrDefault();
            // Skiping number of Rows count
            var start = Request.Form["start"].FirstOrDefault();
            // Paging Length 10,20
            var length = Request.Form["length"].FirstOrDefault();
            // Sort Column Name
            var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            // Sort Column Direction ( asc ,desc)
            var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
            // Search Value from (Search box)
            var searchValue = Request.Form["search[value]"].FirstOrDefault();

            //Paging Size (10,20,50,100)
            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;


            var objResult = (from ct in _context.ClientTrackingInfo
                             join c in _context.Client on ct.ClientId equals c.ClientId
                             join cst in _context.ClientTrackingState on ct.TrackingStateId equals cst.ClientTrackingStateId
                             join cs in _context.ClientTrackingStatus on ct.TrackingStatusId equals cs.ClientTrackingStatusId
                             join u in _context.Users on ct.AssignedTo equals u.UserId
                             where ct.ClientId == clientId && u.UserActiveFlag == true
                             select new
                             {
                                 CallDate = ((DateTime)ct.CallDate).ToString("dd/MM/yyyy"),
                                 ClientName = c.ClientName,
                                 ClientTrackingStateName = cst.ClientTrackingStateName,
                                 ClientTrackingStatusName = cs.ClientTrackingStatusName,
                                 Comment = ct.Comment,
                                 AssignedTo = u.UserName
                             });
           
            //Sorting
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
            {
                objResult = objResult.OrderBy(sortColumn + " " + sortColumnDirection);
            }
            //Search
            if (!string.IsNullOrEmpty(searchValue))
            {
                objResult = objResult.Where(m => m.Comment == searchValue);
            }

            //total number of rows count 
            recordsTotal = objResult.Count();
            //Paging 
            var data = objResult.Skip(skip).Take(pageSize).ToList();
            //Returning Json Data
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data });

           

        }

    }
}