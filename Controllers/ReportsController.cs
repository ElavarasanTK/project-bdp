﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BDPlanning.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq.Dynamic.Core;
using ReflectionIT.Mvc.Paging;
using Microsoft.EntityFrameworkCore;

namespace BDPlanning.Controllers
{
    public class ReportsController : Controller
        
    {
        private readonly BDPlanningContext _context;

        public ReportsController(BDPlanningContext context)
        {
            _context = context;
        }
        // GET: Reports
        public async Task<IActionResult> ClientTrackingReports(int page = 1)
        {
            if (HttpContext.Session.GetString("usrName") != null)
            {
                
                var qry = _context.ClientTrackingInfo.AsNoTracking().OrderBy(p => p.Client.ClientActiveFlag);
                var model = await PagingList.CreateAsync(qry, 5, page);
                return View(model);
            }
            else
            {
                return RedirectToAction("Login", "Users");
            }
        }
        

        public ActionResult MeetingReports()
        {
            return View();
        }
        public void LoadDropDown()
        {
            //load client drop down, state, status and users
            List<Client> lstClient = new List<Client>();
            List<ClientTrackingStatus> lstStatus = new List<ClientTrackingStatus>();
            List<ClientTrackingState> lstState = new List<ClientTrackingState>();
            List<Users> lstUser = new List<Users>();

            Client obClient = new Client();
            obClient.ClientId = 0;
            obClient.ClientName = "--Select Client--";
            lstClient.Add(obClient);

            ClientTrackingStatus obStatus = new ClientTrackingStatus();
            obStatus.ClientTrackingStatusId = 0;
            obStatus.ClientTrackingStatusName = "--Select Tracking Status--";
            lstStatus.Add(obStatus);

            ClientTrackingState obState = new ClientTrackingState();
            obState.ClientTrackingStateId = 0;
            obState.ClientTrackingStateName = "--Select Traking State--";
            lstState.Add(obState);

            Users obUser = new Users();
            obUser.UserId = 0;
            obUser.UserName = "--Select User--";
            lstUser.Add(obUser);

            var objClient = _context.Client
                            .Where(r => r.ClientActiveFlag == true)
                            .Select(r => new { r.ClientId, r.ClientName }).ToList();

            var objStatus = _context.ClientTrackingStatus
                            .Select(r => new { r.ClientTrackingStatusId, r.ClientTrackingStatusName }).ToList();

            var objState = _context.ClientTrackingState
                           .Select(r => new { r.ClientTrackingStateId, r.ClientTrackingStateName }).ToList();

            var objUser = _context.Users
                          .Where(r => r.UserActiveFlag == true)
                          .Select(r => new { r.UserId, r.UserName }).ToList();


            foreach (var r in objClient)
            {
                Client tmpClient = new Client();
                tmpClient.ClientId = r.ClientId;
                tmpClient.ClientName = r.ClientName;
                lstClient.Add(tmpClient);
            }
            foreach (var r in objState)

            {
                ClientTrackingState tmpState = new ClientTrackingState();
                tmpState.ClientTrackingStateId = r.ClientTrackingStateId;
                tmpState.ClientTrackingStateName = r.ClientTrackingStateName;
                lstState.Add(tmpState);
            }
            foreach (var r in objStatus)
            {
                ClientTrackingStatus tmpStatus = new ClientTrackingStatus();
                tmpStatus.ClientTrackingStatusId = r.ClientTrackingStatusId;
                tmpStatus.ClientTrackingStatusName = r.ClientTrackingStatusName;
                lstStatus.Add(tmpStatus);
            }
            foreach (var r in objUser)
            {
                Users tmpUser = new Users();
                tmpUser.UserId = r.UserId;
                tmpUser.UserName = r.UserName;
                lstUser.Add(tmpUser);
            }

            ViewBag.lstClient = lstClient;
            ViewBag.lstStatus = lstStatus;
            ViewBag.lstState = lstState;
            ViewBag.lstUsers = lstUser;
        }
        [HttpPost]
        public IActionResult ClientTrackingReports(DateTime fromDate,DateTime toDate)
        {
                             var objResult = (from ct in _context.ClientTrackingInfo
                             join c in _context.Client on ct.ClientId equals c.ClientId
                             join cst in _context.ClientTrackingState on ct.TrackingStateId equals cst.ClientTrackingStateId
                             join cs in _context.ClientTrackingStatus on ct.TrackingStatusId equals cs.ClientTrackingStatusId
                             join u in _context.Users on ct.AssignedTo equals u.UserId
                             where ct.CallDate <= toDate &&ct.RemainderOn >= fromDate &&  u.UserActiveFlag == true
                             select new
                             {
                                 CallDate = ((DateTime)ct.CallDate).ToString("dd/MM/yyyy"),
                                 ClientName = c.ClientName,
                                 ClientTrackingStateName = cst.ClientTrackingStateName,
                                 ClientTrackingStatusName = cs.ClientTrackingStatusName,
                                 Comment = ct.Comment,
                                 AssignedTo = u.UserName,
                                 FollowUpDate = ct.RemainderOn
                             }).ToList();

            return View(objResult);


        }
    }
}