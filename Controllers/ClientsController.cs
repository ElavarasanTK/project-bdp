﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BDPlanning.Models;
using Microsoft.AspNetCore.Http;
using ReflectionIT.Mvc.Paging;

namespace BDPlanning.Controllers
{
    public class ClientsController : Controller
    {
        private readonly BDPlanningContext _context;

        public ClientsController(BDPlanningContext context)
        {
            _context = context;
        }

        // GET: Clients
        public async Task<IActionResult> Index(int page=1)
        {
            //session check
            if (TempData["userEmail"] != null)
            {
                var qry = _context.Client.AsNoTracking().OrderBy(d => d.ClientActiveFlag);
                var model = await PagingList.CreateAsync(qry, 5, page);
                TempData.Keep("userEmail");
                TempData.Keep("userName");
                //return View(_context.Client.Where(r=>r.ClientActiveFlag==true).ToList());
                return View(model);
            }
            else
            {
                return RedirectToAction("Login", "Users");
            }
        }

        // GET: Clients/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var client = await _context.Client
                .SingleOrDefaultAsync(m => m.ClientId == id);
            if (client == null)
            {
                return NotFound();
            }

            return View(client);
        }

        // GET: Clients/Create
        public IActionResult Create()
        {
            //session check
            if(TempData["userEmail"]!=null)
            {
                TempData.Keep("userEmail");
                TempData.Keep("userName");
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Users");
            }
        }

        // POST: Clients/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ClientId,ClientName,ClientPoc,ReferenceBy,ClientStudy,ClientActiveFlag,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn,ContactEmail,ContactNumber")] Client client)
        {
            //session check
            if (TempData["userEmail"] != null)
            {
                if (ModelState.IsValid)
                {
                    //check if user email/mobile/name is already in the db
                    int returnFlag = 0;
                    var result = _context.Client.Where(r => r.ContactEmail == client.ContactEmail && r.ClientActiveFlag == true ).ToList();
                    
                    if (result.Count() > 0)
                    {
                        ModelState.AddModelError("ContactEmail", "Email ID already exists");

                        returnFlag++;
                    }
                    var resultMobile = _context.Client.Where(r => r.ContactNumber == client.ContactNumber && r.ClientActiveFlag == true ).ToList();
                    if (resultMobile.Count() > 0)
                    {
                        ModelState.AddModelError("ContactNumber", "Mobile number already exists");
                        returnFlag++;
                    }
                    if (returnFlag > 0)
                    {
                        return View(client);
                    }

                    //created by is current session user and created on is current date
                    client.CreatedBy = TempData["userEmail"].ToString();
                    client.CreatedOn = DateTime.Now;
                    _context.Add(client);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                return View(client);
            }

            else
            {
                return RedirectToAction("Login", "Users");
            }
        }

        // GET: Clients/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            //session check
            if (TempData["userEmail"] != null)
            {
                TempData.Keep("userEmail");
                TempData.Keep("userName");
                if (id == null)
                {
                    return NotFound();
                }

                var client = await _context.Client.SingleOrDefaultAsync(m => m.ClientId == id);
                if (client == null)
                {
                    return NotFound();
                }
                return View(client);
            }
            else
            {
                return RedirectToAction("Login", "Users");
            }
        }

        // POST: Clients/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ClientId,ClientName,ClientPoc,ReferenceBy,ClientStudy,ClientActiveFlag,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn,ContactNumber,ContactEmail")] Client client)
        {
            //session check
            if (TempData["userEmail"] != null)
            {

                if (id != client.ClientId)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        //check if user email/mobile/name is already in the db
                        var objClient = _context.Client.SingleOrDefault(r => r.ClientId == id && r.ClientActiveFlag == true);
                        int returnFlag = 0;
                        var result = _context.Client.Where(r => r.ContactEmail == client.ContactEmail && r.ClientId != client.ClientId && r.ClientActiveFlag == true).ToList();
                        if (result.Count() > 0)
                        {
                            ModelState.AddModelError("ContactEmail", "Email ID already exists");

                            returnFlag++;
                        }
                        var resultMobile = _context.Client.Where(r => r.ContactNumber == client.ContactNumber && r.ClientId != client.ClientId && r.ClientActiveFlag == true).ToList();
                        if (resultMobile.Count() > 0)
                        {
                            ModelState.AddModelError("ContactNumber", "Mobile number already exists");
                            returnFlag++;
                        }
                        if (returnFlag > 0)
                        {
                            return View(client);
                        }

                        objClient.ClientName = client.ClientName;
                        objClient.ClientPoc = client.ClientPoc;
                        objClient.ContactEmail = client.ContactEmail;
                        objClient.ContactNumber = client.ContactNumber;
                        objClient.ClientStudy = client.ClientStudy;

                        //created by is current session user and created on is current date
                        objClient.UpdatedBy = TempData["userEmail"].ToString();
                        objClient.UpdatedOn = DateTime.Now;

                        _context.Update(objClient);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!ClientExists(client.ClientId))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction(nameof(Index));
                }
                return View(client);
            }

            else
            {
                return RedirectToAction("Login", "Users");
            }
        }

        // GET: Clients/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            //session check
            if (TempData["userEmail"] != null)
            {

                if (id == null)
                {
                    return NotFound();
                }

                var client = await _context.Client
                    .SingleOrDefaultAsync(m => m.ClientId == id);
                if (client == null)
                {
                    return NotFound();
                }

                client.ClientActiveFlag = false;
                client.UpdatedBy = TempData["userEmail"].ToString();
                client.UpdatedOn = DateTime.Now;

                _context.Update(client);
                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction("Login", "Users");
            }
        }

        // POST: Clients/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var client = await _context.Client.SingleOrDefaultAsync(m => m.ClientId == id);
            _context.Client.Remove(client);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        
        private bool ClientExists(int id)
        {
            return _context.Client.Any(e => e.ClientId == id);
        }
    }
}
