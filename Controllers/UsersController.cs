﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BDPlanning.Models;
using Microsoft.AspNetCore.Session;
using Microsoft.AspNetCore.Http;
using ReflectionIT.Mvc.Paging;

namespace BDPlanning.Controllers
{
    public class UsersController : Controller
    {
        private readonly BDPlanningContext _context;

        string usrName = "";

        public UsersController(BDPlanningContext context)
        {
            _context = context;
        }

        // GET: Users
        public async Task<IActionResult> Index(int page=1)
        {
            if (HttpContext.Session.GetString("usrName") != null)
            {
                var qry = _context.Users.AsNoTracking().OrderBy(p => p.UserActiveFlag);
                var model = await PagingList.CreateAsync(qry, 5, page);
                return View(model);
                //return View(_context.Users.Where(r => r.UserActiveFlag == true).ToList());
            }
            else
            {
                return RedirectToAction(nameof(Login));
            }
        }

        // GET: Users/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var users = await _context.Users
                .SingleOrDefaultAsync(m => m.UserId == id);
            if (users == null)
            {
                return NotFound();
            }

            return View(users);
        }

        #region CreateUsers
        // GET: Users/Create
        public IActionResult Create()
        {
            if (HttpContext.Session.GetString("usrName") != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction(nameof(Login));
            }
        }


        // POST: Users/Create
     
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("UserId,UserName,UserEmail,UserMobileNumber,UserActiveFlag,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn")] Users users)
        {
            if (ModelState.IsValid)
            {
                //check session
                if (HttpContext.Session.GetString("usrName") != null)
                {
                    //check if user email/mobile/name is already in the db
                    int returnFlag = 0;
                    var result = _context.Users.Where(r => r.UserEmail == users.UserEmail && r.UserActiveFlag == true).ToList();
                    if (result.Count() > 0)
                    {
                        ModelState.AddModelError("UserEmail", "Email ID already exists");

                        returnFlag++;
                    }
                    var resultMobile = _context.Users.Where(r => r.UserMobileNumber == users.UserMobileNumber && r.UserActiveFlag == true).ToList();
                    if (resultMobile.Count() > 0)
                    {
                        ModelState.AddModelError("UserMobileNumber", "Mobile number already exists");
                        returnFlag++;
                    }
                    if (returnFlag>0)
                    {
                        return View(users);
                    }

                    //created by is current session user and created on is current date
                    users.CreatedBy = HttpContext.Session.GetString("usrName");
                    users.CreatedOn = DateTime.Now;
                    //password is set to default password 
                    users.Password = "password1";
                    _context.Add(users);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return RedirectToAction(nameof(Login));
                }
            }
            return View(users);

        }
        #endregion

        #region Login
        //GET: Users/Login
        public IActionResult Login()
        {
            return View();
        }

        //POST: Users/Login
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login([Bind("UserEmail,Password")] Users user)
        {
            if(ModelState.IsValid)
            {
                //change to email id
                var result = _context.Users.Where(r => r.UserEmail == user.UserEmail && r.Password == user.Password && r.UserActiveFlag == true).ToList();
                if(result.Count() > 0)
                {

                    foreach (var r in result)
                    {
                        HttpContext.Session.SetString("usrName", r.UserName);
                        HttpContext.Session.SetString("usrEmail", r.UserEmail);
                    }
                    TempData["userEmail"] = HttpContext.Session.GetString("usrEmail");
                    TempData["userName"] = HttpContext.Session.GetString("usrName");
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    return View();
                }
            }
            return View();
        }

        #endregion

        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            //Check session
            if (HttpContext.Session.GetString("usrName") != null)
            {
                if (id == null)
                {
                    return NotFound();
                }

                var users = await _context.Users.SingleOrDefaultAsync(m => m.UserId == id);
                if (users == null)
                {
                    return NotFound();
                }
                return View(users);
            }
            else
            {
                return RedirectToAction(nameof(Login));
            }
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("UserId,UserName,UserEmail,UserMobileNumber,UserActiveFlag,CreatedBy,CreatedOn,UpdatedBy,UpdatedOn")] Users users)
        {
            if (HttpContext.Session.GetString("usrName") != null)
            {
                if (id != users.UserId)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        //Email id and mobile number check
                        var objUser = _context.Users.SingleOrDefault(r => r.UserId == id && r.UserActiveFlag == true);
                        int returnFlag = 0;
                        var result = _context.Users.Where(r => r.UserEmail == users.UserEmail && r.UserId!=users.UserId && r.UserActiveFlag == true).ToList();
                        if (result.Count() > 0)
                        {
                            ModelState.AddModelError("UserEmail", "Email ID already exists");

                            returnFlag++;
                        }
                        var resultMobile = _context.Users.Where(r => r.UserMobileNumber == users.UserMobileNumber && r.UserId != users.UserId && r.UserActiveFlag == true).ToList();
                        if (resultMobile.Count() > 0)
                        {
                            ModelState.AddModelError("UserMobileNumber", "Mobile number already exists");
                            returnFlag++;
                        }
                        if (returnFlag > 0)
                        {
                            return View(users);
                        }

                        objUser.UserName = users.UserName;
                        objUser.UserMobileNumber = users.UserMobileNumber;
                        objUser.UserEmail = users.UserEmail;

                        objUser.UpdatedBy = HttpContext.Session.GetString("usrName");
                        objUser.UpdatedOn = DateTime.Now;
                    
                        _context.Update(objUser);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!UsersExists(users.UserId))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction(nameof(Index));
                }
                return View(users);
            }
            else
            {
                return RedirectToAction(nameof(Login));
            }
        }

        // GET: Users/Delete/5
      
        public async Task<IActionResult> Delete(int? id)
        {
            if (HttpContext.Session.GetString("usrName") != null)
            {

                if (id == null)
                {
                    return NotFound();
                }
                try
                {
                    var users = await _context.Users
                        .SingleOrDefaultAsync(m => m.UserId == id);
                    if (users == null)
                    {
                        return NotFound();
                    }

                    users.UserActiveFlag = false;
                    users.UpdatedBy = HttpContext.Session.GetString("usrName");
                    users.UpdatedOn = DateTime.Now;

                    _context.Update(users);
                    await _context.SaveChangesAsync();
                }

                catch (DbUpdateConcurrencyException)
                {
                    throw;
                }


                return RedirectToAction(nameof(Index));
            }
     
                else
                {
                return RedirectToAction(nameof(Login));
                }
        }

       

        private bool UsersExists(int id)
        {
            return _context.Users.Any(e => e.UserId == id);
        }

        public ActionResult Logout()
        {
            HttpContext.Session.Clear();
            return View();
        }
    }
}
