﻿using System;
using System.Collections.Generic;

namespace BDPlanning.Models
{
    public partial class MeetingType
    {
        public MeetingType()
        {
            ClientMeeting = new HashSet<ClientMeeting>();
        }

        public int MeetingTypeId { get; set; }
        public string MeetingTypeName { get; set; }

        public ICollection<ClientMeeting> ClientMeeting { get; set; }
    }
}
