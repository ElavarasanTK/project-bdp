﻿using System;
using System.Collections.Generic;

namespace BDPlanning.Models
{
    public partial class Users
    {
        public Users()
        {
            ClientMeeting = new HashSet<ClientMeeting>();
            ClientTrackingInfo = new HashSet<ClientTrackingInfo>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string UserEmail { get; set; }
        public string UserMobileNumber { get; set; }
        public bool UserActiveFlag { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string Password { get; set; }

        public ICollection<ClientMeeting> ClientMeeting { get; set; }
        public ICollection<ClientTrackingInfo> ClientTrackingInfo { get; set; }
    }
}
