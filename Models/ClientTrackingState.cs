﻿using System;
using System.Collections.Generic;

namespace BDPlanning.Models
{
    public partial class ClientTrackingState
    {
        public ClientTrackingState()
        {
            ClientTrackingInfo = new HashSet<ClientTrackingInfo>();
        }

        public int ClientTrackingStateId { get; set; }
        public string ClientTrackingStateName { get; set; }

        public ICollection<ClientTrackingInfo> ClientTrackingInfo { get; set; }
    }
}
