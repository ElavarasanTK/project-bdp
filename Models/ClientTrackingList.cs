﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BDPlanning.Models
{
    public class ClientTrackingList
    {
        public string callDate;
        public string clientName;
        public string clientState;
        public string clientStatus;
        public string comments;
        public string assignedTo;
    }
}
