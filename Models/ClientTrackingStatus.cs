﻿using System;
using System.Collections.Generic;

namespace BDPlanning.Models
{
    public partial class ClientTrackingStatus
    {
        public ClientTrackingStatus()
        {
            ClientTrackingInfo = new HashSet<ClientTrackingInfo>();
        }

        public int ClientTrackingStatusId { get; set; }
        public string ClientTrackingStatusName { get; set; }

        public ICollection<ClientTrackingInfo> ClientTrackingInfo { get; set; }
    }
}
