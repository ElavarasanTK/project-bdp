﻿using System;
using System.Collections.Generic;

namespace BDPlanning.Models
{
    public partial class ClientTrackingRemainder
    {
        public int ClientTrackingRemainderId { get; set; }
        public int? ClientTrackingId { get; set; }
        public DateTime? ClientTrackingRemainderOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public ClientTrackingInfo ClientTracking { get; set; }
    }
}
