﻿using System;
using System.Collections.Generic;

namespace BDPlanning.Models
{
    public partial class ClientRemainder
    {
        public int ClientRemainderId { get; set; }
        public int? ClientId { get; set; }
        public DateTime? ClientRemainderOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public Client Client { get; set; }
    }
}
