﻿using System;
using System.Collections.Generic;

namespace BDPlanning.Models
{
    public partial class ClientTrackingInfo
    {
        public ClientTrackingInfo()
        {
            ClientTrackingRemainder = new HashSet<ClientTrackingRemainder>();
        }

        public int ClientTrackingId { get; set; }
        public int? ClientId { get; set; }
        public int? TrackingStateId { get; set; }
        public int? TrackingStatusId { get; set; }
        public string Comment { get; set; }
        public int? AssignedTo { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public DateTime? RemainderOn { get; set; }
        public DateTime? CallDate { get; set; }

        public Users AssignedToNavigation { get; set; }
        public Client Client { get; set; }
        public ClientTrackingState TrackingState { get; set; }
        public ClientTrackingStatus TrackingStatus { get; set; }
        public ICollection<ClientTrackingRemainder> ClientTrackingRemainder { get; set; }
    }
}
