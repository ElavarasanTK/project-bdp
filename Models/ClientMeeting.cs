﻿using System;
using System.Collections.Generic;

namespace BDPlanning.Models
{
    public partial class ClientMeeting
    {
        public ClientMeeting()
        {
            ClientMeetingRemainder = new HashSet<ClientMeetingRemainder>();
        }

        public int MeetingId { get; set; }
        public int? ClientId { get; set; }
        public string Location { get; set; }
        public string MeetingPoc { get; set; }
        public string MeetingPocEmail { get; set; }
        public string MeetingPocNumber { get; set; }
        public string MeetingPocDesig { get; set; }
        public int? UserId { get; set; }
        public int? MeetingTypeId { get; set; }
        public string MeetingMom { get; set; }
        public bool HasRemainderFlag { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public DateTime? MeetingDate { get; set; }
        public string MeetingName { get; set; }
        public DateTime? RemainderOn { get; set; }
        public bool? DeleteFlag { get; set; }

        public Client Client { get; set; }
        public MeetingType MeetingType { get; set; }
        public Users User { get; set; }
        public ICollection<ClientMeetingRemainder> ClientMeetingRemainder { get; set; }
    }
}
