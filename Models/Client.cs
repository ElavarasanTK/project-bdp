﻿using System;
using System.Collections.Generic;

namespace BDPlanning.Models
{
    public partial class Client
    {
        public Client()
        {
            ClientMeeting = new HashSet<ClientMeeting>();
            ClientRemainder = new HashSet<ClientRemainder>();
            ClientTrackingInfo = new HashSet<ClientTrackingInfo>();
        }

        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string ClientPoc { get; set; }
        public string ReferenceBy { get; set; }
        public string ClientStudy { get; set; }
        public bool ClientActiveFlag { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public string ContactEmail { get; set; }
        public string ContactNumber { get; set; }

        public ICollection<ClientMeeting> ClientMeeting { get; set; }
        public ICollection<ClientRemainder> ClientRemainder { get; set; }
        public ICollection<ClientTrackingInfo> ClientTrackingInfo { get; set; }
    }
}
