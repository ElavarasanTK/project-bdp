﻿
using System.ComponentModel.DataAnnotations;

namespace BDPlanning.Models
{
    public class UserLogin
    {
        [Required]
        [Display (Name = "User Name")]
        public string userName;
        [Required]
        [Display(Name = "Password")]
        public string password;
    }
}
