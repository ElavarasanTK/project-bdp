﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BDPlanning.Models
{
    public partial class BDPlanningContext : DbContext
    {
        public virtual DbSet<Client> Client { get; set; }
        public virtual DbSet<ClientMeeting> ClientMeeting { get; set; }
        public virtual DbSet<ClientMeetingRemainder> ClientMeetingRemainder { get; set; }
        public virtual DbSet<ClientRemainder> ClientRemainder { get; set; }
        public virtual DbSet<ClientTrackingInfo> ClientTrackingInfo { get; set; }
        public virtual DbSet<ClientTrackingRemainder> ClientTrackingRemainder { get; set; }
        public virtual DbSet<ClientTrackingState> ClientTrackingState { get; set; }
        public virtual DbSet<ClientTrackingStatus> ClientTrackingStatus { get; set; }
        public virtual DbSet<MeetingType> MeetingType { get; set; }
        public virtual DbSet<Users> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer(@"Data Source=DESKTOP-GHJLLPI;Initial Catalog=BDPlanning;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>(entity =>
            {
                entity.Property(e => e.ClientName)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.ClientPoc)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ClientStudy).IsUnicode(false);

                entity.Property(e => e.ContactEmail)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ContactNumber)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ReferenceBy)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");
            });

            modelBuilder.Entity<ClientMeeting>(entity =>
            {
                entity.HasKey(e => e.MeetingId);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Location)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MeetingDate).HasColumnType("datetime");

                entity.Property(e => e.MeetingMom)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.MeetingName)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.MeetingPoc)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MeetingPocDesig)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MeetingPocEmail)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.MeetingPocNumber)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RemainderOn).HasColumnType("datetime");

                entity.Property(e => e.UpdatedBy)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.ClientMeeting)
                    .HasForeignKey(d => d.ClientId)
                    .HasConstraintName("FK__ClientMee__Clien__25869641");

                entity.HasOne(d => d.MeetingType)
                    .WithMany(p => p.ClientMeeting)
                    .HasForeignKey(d => d.MeetingTypeId)
                    .HasConstraintName("FK__ClientMee__Meeti__276EDEB3");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.ClientMeeting)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK__ClientMee__UserI__267ABA7A");
            });

            modelBuilder.Entity<ClientMeetingRemainder>(entity =>
            {
                entity.Property(e => e.ClientMeetingRemainderOn).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.UpdatedBy)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.ClientMeeting)
                    .WithMany(p => p.ClientMeetingRemainder)
                    .HasForeignKey(d => d.ClientMeetingId)
                    .HasConstraintName("FK__ClientMee__Clien__2A4B4B5E");
            });

            modelBuilder.Entity<ClientRemainder>(entity =>
            {
                entity.Property(e => e.ClientRemainderOn).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.UpdatedBy)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.ClientRemainder)
                    .HasForeignKey(d => d.ClientId)
                    .HasConstraintName("FK__ClientRem__Clien__1A14E395");
            });

            modelBuilder.Entity<ClientTrackingInfo>(entity =>
            {
                entity.HasKey(e => e.ClientTrackingId);

                entity.Property(e => e.ClientTrackingId).HasColumnName("ClientTRackingId");

                entity.Property(e => e.CallDate).HasColumnType("datetime");

                entity.Property(e => e.Comment)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.RemainderOn).HasColumnType("datetime");

                entity.Property(e => e.UpdatedBy)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.AssignedToNavigation)
                    .WithMany(p => p.ClientTrackingInfo)
                    .HasForeignKey(d => d.AssignedTo)
                    .HasConstraintName("FK__ClientTra__Assig__1FCDBCEB");

                entity.HasOne(d => d.Client)
                    .WithMany(p => p.ClientTrackingInfo)
                    .HasForeignKey(d => d.ClientId)
                    .HasConstraintName("FK__ClientTra__Clien__1CF15040");

                entity.HasOne(d => d.TrackingState)
                    .WithMany(p => p.ClientTrackingInfo)
                    .HasForeignKey(d => d.TrackingStateId)
                    .HasConstraintName("FK__ClientTra__Track__1DE57479");

                entity.HasOne(d => d.TrackingStatus)
                    .WithMany(p => p.ClientTrackingInfo)
                    .HasForeignKey(d => d.TrackingStatusId)
                    .HasConstraintName("FK__ClientTra__Track__1ED998B2");
            });

            modelBuilder.Entity<ClientTrackingRemainder>(entity =>
            {
                entity.Property(e => e.ClientTrackingRemainderOn).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.UpdatedBy)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.HasOne(d => d.ClientTracking)
                    .WithMany(p => p.ClientTrackingRemainder)
                    .HasForeignKey(d => d.ClientTrackingId)
                    .HasConstraintName("FK__ClientTra__Clien__22AA2996");
            });

            modelBuilder.Entity<ClientTrackingState>(entity =>
            {
                entity.Property(e => e.ClientTrackingStateName)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ClientTrackingStatus>(entity =>
            {
                entity.Property(e => e.ClientTrackingStatusName)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MeetingType>(entity =>
            {
                entity.Property(e => e.MeetingTypeName)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Password)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedOn).HasColumnType("datetime");

                entity.Property(e => e.UserEmail)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.UserMobileNumber)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });
        }
    }
}
