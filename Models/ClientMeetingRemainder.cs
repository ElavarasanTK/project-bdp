﻿using System;
using System.Collections.Generic;

namespace BDPlanning.Models
{
    public partial class ClientMeetingRemainder
    {
        public int ClientMeetingRemainderId { get; set; }
        public int? ClientMeetingId { get; set; }
        public DateTime? ClientMeetingRemainderOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedOn { get; set; }

        public ClientMeeting ClientMeeting { get; set; }
    }
}
